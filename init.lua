--

local MODNAME = minetest.get_current_modname()
local WORLDPATH = minetest.get_worldpath()

local function log (lvl, ...)
   minetest.log(lvl, "[watchdog] " .. string.format(...))
end

-- load settings
local settings = minetest.settings
if not settings:get_bool("mtwatchdog.enable", true) then
   log("action", "watchdog disabled, bye")
   return
end
local NOTIFY_DELAY = tonumber(settings:get("mtwatchdog.delay")) or 60
local PID_FILE = settings:get("mtwatchdog.pid_file") or "mtrun.pid"

log("action", "starting mtwatchdog")
log("action", "delay: %ds", tostring(NOTIFY_DELAY))
log("action", "pid_file: '%s'", PID_FILE)

-- get os.execute()
local IE = minetest.request_insecure_environment()
if not IE then
   log("error",
       "could not get an insecure environment, please add 'secure.trusted_mods = %s' in minetest.conf",
       MODNAME)
   return
end
local execute = IE.os.execute
IE = nil

-- read pid
local pid_path = WORLDPATH .. "/" .. PID_FILE
log("action", "reading pid from '%s'", pid_path)
local f = io.open(pid_path)
if not f then
   log("error", "could not read '%s'", pid_path)
   return
end
local spid = f:read("*a")
f:close()
local pid = tonumber(spid)
if not pid then
   log("error", "invalid pid found in %s: '%s'", pid_path, spid)
   return
end
log("action", "found PID: '%d'", pid)

-- go
local notify_cmd = string.format("systemd-notify --pid=%d WATCHDOG=1", pid)
local function notify ()
   log("info", "> " .. notify_cmd)
   execute(notify_cmd)
   minetest.after(NOTIFY_DELAY, notify)
end
notify()
